#include <pthread.h>
#include <dlfcn.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/syscall.h>
#include <sys/time.h>
#include <string.h>

#define SIZE_TAB 500
#define NB_THREAD_MUTEX 200
#define NB_OPE_MUTEX 100

double temps_avant_cpu = 0;

//APPLICATION 2

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

void swap(int *xp, int *yp)  
{  
    int temp = *xp;  
    *xp = *yp;  
    *yp = temp;  
}  
   
void bubbleSort(int arr[], int n)  
{  
    int i, j;  
    for (i = 0; i < n-1; i++){     
    	for (j = 0; j < n-i-1; j++){  
        	if (arr[j] > arr[j+1]){
            		swap(&arr[j], &arr[j+1]);  
        	}
        }
    }
}  

void operation(){
	int tab[SIZE_TAB];
	int i;
	for(i=0; i<SIZE_TAB; i++){
		tab[i]=rand()%321;
	}
	bubbleSort(tab,SIZE_TAB);
}

void job_useless(int nb_operation){
	int i;
	for(i = 0 ; i<nb_operation;i++){
		operation();
	}
}

void* job_verrou(){

	pthread_mutex_lock(&mutex);
	printf("[THREAD VERROU] Début : %ld\n", syscall(__NR_gettid));

	job_useless(NB_OPE_MUTEX/4);

	printf("[THREAD VERROU] Liberation des autres threads\n");
	fflush(stdin);

	pthread_mutex_unlock(&mutex);

	printf("[THREAD] Fin : %ld\n", syscall(__NR_gettid));
	fflush(stdin);
	pthread_exit(NULL);

}

void* job_bloquant(){
	struct timeval ts;
	struct timeval ts2;

	printf("[THREAD] avant lock : %ld\n", syscall(__NR_gettid));

	gettimeofday(&ts,NULL);
	pthread_mutex_lock(&mutex);
  	gettimeofday(&ts2,NULL);
	pthread_mutex_unlock(&mutex);

  	double res = (double)(ts.tv_sec*1000000 + ts.tv_usec);
  	double res2 = (double)(ts2.tv_sec*1000000 + ts2.tv_usec);
	printf("[THREAD] Temps d'attente de %ld sur le wait : %f secondes\n",
	        syscall(__NR_gettid),(((double)(res2-res))/1000000));
	fflush(stdin);

	job_useless(NB_OPE_MUTEX/4);

	printf("[THREAD] Fin : %ld\n", syscall(__NR_gettid));
	fflush(stdin);

	pthread_exit(NULL);

}

int main (){
	srand (time (NULL));
	int i;
	int err;
	sleep(20);
	printf("[APPLICATION 2] On commence !\n");
	fflush(stdin);
	pthread_t tab[NB_THREAD_MUTEX+1];

	/* Création du thread qui libèrera tous les autres */
	if((err = pthread_create(&tab[0], NULL, job_verrou,
	        (void *)NULL)) != 0){
		printf("Echec de la création du thread: [%s]", strerror(err));
		return EXIT_FAILURE;;
	}	

	sleep(2);

	for(i=1; i<NB_THREAD_MUTEX+1; i++){
		if((err = pthread_create(&tab[i], NULL, job_bloquant,(void *)NULL)) != 0){
			printf("Echec de la création du thread: [%s]", strerror(err));
			return EXIT_FAILURE;;
		}
	}
	for(i=0; i<NB_THREAD_MUTEX+1; i++){
		pthread_join(tab[i], NULL);
	}

	printf("[APPLICATION 2] On a fini\n");
	fflush(stdin);

	return 0;
}
