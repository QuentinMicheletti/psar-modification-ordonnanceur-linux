#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/time.h>

int main(int argc, char** argv) {
    FILE *file = fopen("perf.txt", "a");
    if(!file){
        fprintf(stderr, "L'ouverture du fichier a échoué\n");
        return -1;
    }
    struct timeval debut;
    struct timeval fin;
    gettimeofday(&debut,NULL);
    if(!fork()){
        fclose(file);
        execv("./appli1",NULL);
    }
    if(!fork()) {
        fclose(file);
        execv("./appli2", NULL);
    }
    wait(NULL);
    wait(NULL);
    gettimeofday(&fin, NULL);
    long tmp_sec =  fin.tv_sec-debut.tv_sec;
    long tmp_nanos = fin.tv_usec - debut.tv_sec;
    if(tmp_nanos < 0) {
        tmp_nanos = 0 - tmp_nanos;
        tmp_sec -= 1;
    }
    printf("Temps total exécution : %ld s\n", tmp_sec);
    fprintf(file,"%lu\n", tmp_sec);
    fclose(file);
    return 0;
}
