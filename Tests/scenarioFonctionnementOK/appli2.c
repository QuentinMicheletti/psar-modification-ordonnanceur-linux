#include <pthread.h>
#include <dlfcn.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/syscall.h>
#include <sys/time.h>
#include <string.h>

#define SIZE_TAB 500
//#define NB_THREAD_MUTEX 50
#define NB_OPE_MUTEX 100

int nb_waiters;

//APPLICATION 2

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutex2 = PTHREAD_MUTEX_INITIALIZER;

void swap(int *xp, int *yp)  
{  
    int temp = *xp;  
    *xp = *yp;  
    *yp = temp;  
}  
   
void bubbleSort(int arr[], int n)  
{  
    int i, j;  
    for (i = 0; i < n-1; i++){     
    	for (j = 0; j < n-i-1; j++){  
        	if (arr[j] > arr[j+1]){
            		swap(&arr[j], &arr[j+1]);  
        	}
        }
    }
}  

void operation(){
	int tab[SIZE_TAB];
	int i;
	for(i=0; i<SIZE_TAB; i++){
		tab[i]=rand()%321;
	}
	bubbleSort(tab,SIZE_TAB);
}

void job_useless(int nb_operation){
	int i;
	for(i = 0 ; i<nb_operation;i++){
		operation();
	}
}

void* job_verrou(){
	struct timeval ts;
	struct timeval ts2;
  	double res;
  	double res2;

   	FILE *file = fopen("perf.txt", "a");
   	if(!file){
        	fprintf(stderr, "L'ouverture du fichier a échoué\n");
		pthread_exit(NULL);
    	}
   	FILE *file2 = fopen("perf2.txt", "a");
   	if(!file2){
        	fprintf(stderr, "L'ouverture du fichier a échoué\n");
		pthread_exit(NULL);
    	}
   	FILE *file3 = fopen("perf3.txt", "a");
   	if(!file3){
        	fprintf(stderr, "L'ouverture du fichier a échoué\n");
		pthread_exit(NULL);
    	}
	
	//Traitement 1 avec verrous et waiters
	pthread_mutex_lock(&mutex);
	printf("[THREAD VERROU] Début : %ld\n", syscall(__NR_gettid));
	
	gettimeofday(&ts,NULL);
	job_useless(NB_OPE_MUTEX/4); //Traitement nécessitant plusieurs commutations
	gettimeofday(&ts2,NULL);

  	res = (double)(ts.tv_sec*1000000 + ts.tv_usec);
  	res2 = (double)(ts2.tv_sec*1000000 + ts2.tv_usec);
	printf("[THREAD VERROU] Traitement 1 avec verrou et %d waiters de %ld : %f secondes\n", nb_waiters, syscall(__NR_gettid),(((double)(res2-res))/1000000));

	printf("[THREAD VERROU] Liberation des autres threads\n");
	fprintf(file, "%f\n", (((double)(res2-res))/1000000));
	fclose(file);
	pthread_mutex_unlock(&mutex);

	if(nb_waiters == 5){
		//Traitement 2 sans verrou
		gettimeofday(&ts,NULL);
		job_useless(NB_OPE_MUTEX/4); //Traitement nécessitant plusieurs commutations
		gettimeofday(&ts2,NULL);

	  	res = (double)(ts.tv_sec*1000000 + ts.tv_usec);
	  	res2 = (double)(ts2.tv_sec*1000000 + ts2.tv_usec);
		printf("[THREAD VERROU] Traitement 2 sans verrou de %ld : %f secondes\n", syscall(__NR_gettid),(((double)(res2-res))/1000000));
		fprintf(file2, "%f\n", (((double)(res2-res))/1000000));
		fclose(file2);

		//Traitement 3 avec verrou sans waiters
		pthread_mutex_lock(&mutex2);

		gettimeofday(&ts,NULL);
		job_useless(NB_OPE_MUTEX/4); //Traitement nécessitant plusieurs commutation
		gettimeofday(&ts2,NULL);

	  	res = (double)(ts.tv_sec*1000000 + ts.tv_usec);
	  	res2 = (double)(ts2.tv_sec*1000000 + ts2.tv_usec);
		printf("[THREAD VERROU] Traitement 3 avec verrou et 0 waiters de %ld : %f secondes\n", syscall(__NR_gettid),(((double)(res2-res))/1000000));

		fprintf(file3, "%f\n", (((double)(res2-res))/1000000));
		fclose(file3);
		pthread_mutex_unlock(&mutex2);
	}
	else{
		fclose(file2);
		fclose(file3);	
	}

	printf("[THREAD VERROU] Fin : %ld\n", syscall(__NR_gettid));
	fflush(stdin);
	pthread_exit(NULL);
}
 
void* job_bloquant(){
	pthread_mutex_lock(&mutex);
	pthread_mutex_unlock(&mutex);

	printf("[THREAD] Fin : %ld\n", syscall(__NR_gettid));
	fflush(stdin);

	pthread_exit(NULL);
}

int main (int argc, char *argv[]){
	srand (time (NULL));
	int i;
	int err;
	printf("[APPLICATION 2] On commence !\n");
	fflush(stdin);

	if(argc != 2){
		printf("Le nombre d'agurments n'est pas correct\n");
		return -1;
	}

	nb_waiters = atoi(argv[1]);
	pthread_t tab[nb_waiters+1];

	/* Création du thread qui libèrera tous les autres */
	if((err = pthread_create(&tab[0], NULL, job_verrou,(void *)NULL)) != 0){
		printf("Echec de la création du thread: [%s]", strerror(err));
		return EXIT_FAILURE;;
	}	

	sleep(2);

	for(i=1; i<nb_waiters+1; i++){
		if((err = pthread_create(&tab[i], NULL, job_bloquant,(void *)NULL)) != 0){
			printf("Echec de la création du thread: [%s]", strerror(err));
			return EXIT_FAILURE;;
		}
	}

	for(i=0; i<nb_waiters+1; i++){
		pthread_join(tab[i], NULL);
	}

	printf("[APPLICATION 2] On a fini\n");
	fflush(stdin);

	return 0;
}
