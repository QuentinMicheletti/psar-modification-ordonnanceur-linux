#include <pthread.h>
#include <dlfcn.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/syscall.h>
#include <sys/time.h>
#include <string.h>

#define SIZE_TAB 500
#define NB_THREAD_AUTRE 1000
#define NB_OPE_AUTRE 10000

//APPLICATION 1

void swap(int *xp, int *yp)  
{  
    int temp = *xp;  
    *xp = *yp;  
    *yp = temp;  
}  
   
void bubbleSort(int arr[], int n)  
{  
    int i, j;  
    for (i = 0; i < n-1; i++){     
    	for (j = 0; j < n-i-1; j++){  
        	if (arr[j] > arr[j+1]){
            		swap(&arr[j], &arr[j+1]);  
        	}
        }
    }
}  

void operation(){
	int tab[SIZE_TAB];
	int i;
	for(i=0; i<SIZE_TAB; i++){
		tab[i]=rand()%321;
	}
	bubbleSort(tab,SIZE_TAB);
}

void job_useless(int nb_operation){
	while(1){
		operation();
	}
}

// ICI LES FONCTIONS AUTRE

void* job_autre(){
	job_useless(NB_OPE_AUTRE);
	printf("thread a fini\n");
	pthread_exit(NULL);
}

int main (){
	int i;
	int err;
	printf("[APPLICATION 1] On commence !\n");
	fflush(stdin);
	pthread_t tab2[NB_THREAD_AUTRE];

	int cpt = 0;

	for(i=0; i<NB_THREAD_AUTRE; i++){
		if((err = pthread_create(&tab2[i], NULL, job_autre,(void *)NULL)) != 0){
			printf("Echec de la création du thread: [%s]", strerror(err));
			return EXIT_FAILURE;;
		}
		cpt++;
		if(cpt%100 == 0){
			printf("nb de threads créés : %d\n", cpt);
		}
	}

	printf("[APPLICATION 1] A fini de créer tous les threads !\n");
	//system("./appli2");
	// ATTENTION LE JOIN IMPLIQUE UN FUTEX WAIT SUR LE THREAD QU'ON VEUT JOIN
/*
	for(i=0; i<NB_THREAD_AUTRE; i++){
		pthread_join(tab2[i], NULL);
	}
*/
	while(1){ //CTRL C pour terminer
		sleep(10);
	}
	printf("[APPLICATION 1] On a fini\n");
	fflush(stdin);

	return 0;
}
