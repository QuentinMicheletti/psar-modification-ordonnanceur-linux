#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>

struct mem_test{
    unsigned int val;
    pthread_mutex_t mutex;
};
struct mem_test *mem;
int main(int argc, char **argv){
    int shmid = shmget(IPC_PRIVATE, sizeof(struct mem_test), 0666);
    printf("PID PERE : %d\n", getpid());
    if(!fork()){
        mem = (struct mem_test *) shmat(shmid, 0, 0);
        printf("PID 1 : %d\n", getpid());
        pthread_mutex_lock(&mem->mutex);
        sleep(20);
        pthread_mutex_unlock(&mem->mutex);
        printf("FINI 1\n");
        return 0;
    }
    sleep(1);
    if(!fork()){
        mem = (struct mem_test *) shmat(shmid, 0, 0);
        printf("PID 2 : %d\n", getpid());
        pthread_mutex_lock(&mem->mutex);
        sleep(20);
        pthread_mutex_unlock(&mem->mutex);
        printf("FINI 2\n");
        return 0;
    }
    wait(NULL);
    wait(NULL);
    return 0;
}
